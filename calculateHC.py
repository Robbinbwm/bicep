#Author:            Robbin Bouwmeester (supervisors: Erik van Dijk & Sanne Abeln, feedback and server implementation: Bernd Brandt)
#Purpose:           Wrapper for calculating the linear increase in heat capicity of folded proteins
#                   also plotting data and getting some statistics
#Date:              19-06-2014
#Python version:    2.7.5
#Packages:          MatplotLib V1.3.1
#Bugs:              No bugs found...

#Set MPL dir
import os, os.path
import sys

mplconfigdir = '/scratch/jobs/'
if mplconfigdir!=None: os.environ['MPLCONFIGDIR'] = mplconfigdir

#Set right folder matplotlib
import matplotlib
matplotlib.use('Agg')
import matplotlib.patches as mpatches
#Imports
import numpy
from matplotlib import pyplot
from math import fabs


"""Function that plots the linear increase in heat capacity of a folded protein.
        Inputs:
            HSA     - a float with the hydrophobic surface area in Angstroms
            mw      - the molecular weight
            makePlot- boolean, should we make a plot?
            plotFile- optionally define an output file
            show    - boolean, show the plot in a graphical window
            startT  - start temperature in Celsius to plot
            stopT   - stop temperature in Celsius to plot
        Outputs:
            formula - a string that contains a [FLOAT]*(273.15-T)+[FLOAT]
"""
def calculateFormulaHC(HSA,mw,makePlot=True,plotFile="out",plotTitle="",show=False,startT=260,stopT=380,ticksteps=20,slopecoefficient=(0.09003/1000),interceptcoefficient=(1.31/1000),
                       mwSlope=(0.0067/1000),mwIntercept=(1.323/1000),celsius=False,heatcapFile="",plotMolWeight=True,tRef=0.0,cRef=0.0,slopeT=(270,290),slopeC=(10,15)):	
    #Is the start temperature higher than stop?
    if startT >= stopT:
        sys.stderr.write("Cannot have a starting range that is lower than the stopping range\n")
        exit(-1)
    if makePlot == True and plotFile == "":
        sys.stderr.write("Please specify a name for the plot\n")
        exit(-1)
        
    #Make a vector with temperatures
    if celsius: x = numpy.linspace(startT,stopT,ticksteps)
    else:       x = numpy.linspace(startT,stopT,ticksteps)
    
    #Make a vector with heat capacity values
    if celsius: y = HSA*slopecoefficient*x+interceptcoefficient*mw
    elif tRef != 0.0: y = HSA*slopecoefficient*x+(cRef-(HSA*slopecoefficient*tRef))
    else:       y = HSA*slopecoefficient*x+((interceptcoefficient*mw)-(HSA*slopecoefficient*273.15))
    
    x2 = []
    y2 = []
    #Do we need to plot the freire baseline? Do the same procedure as above
    if plotMolWeight:    
        if celsius: x2 = numpy.linspace(startT,stopT,ticksteps)
        else:       x2 = numpy.linspace(startT,stopT,ticksteps)
    
        if celsius: y2 = mw*mwSlope*x2+mwIntercept*mw
        elif tRef != 0.0: y2 = mw*mwSlope*x2+(cRef-(mw*mwSlope*tRef))
        else:       y2 = mw*mwSlope*x2+((mwIntercept*mw)-(mw*mwSlope*273.15))

    #Should we make a plot
    if makePlot == True: plotHeatCapacity(x,y,plotFile=plotFile,show=show,plotTitle=plotTitle,\
                                          celsius=False,x2=x2,y2=y2,heatcapFile=heatcapFile,\
                                          tRef=tRef,cRef=cRef)

    #If a reference temperature was given, make sure we fix the intercept. Otherwise also calculate the intercept
    if tRef != 0.0:
        if plotMolWeight: 
            hsaForm = ("<i>%.2f%s18.9 &middot; (273.15 - T) + %.0f</i>" % (HSA*(slopecoefficient*1000),"&plusmn;",((cRef*1000)-((tRef-273.15)*(HSA*(slopecoefficient*1000))))))
            mwForm = ("<i>%.2f%s13 &middot; (273.15 - T) + %.0f</i>" % (mw*(mwSlope*1000),"&plusmn;",((cRef*1000)-((tRef-273.15)*(mw*(mwSlope*1000))))))
            return hsaForm,mwForm
        else: return ("<i>%.2f%s18.9 &middot; (273.15 - T) + %.0f</i>" % (HSA*(slopecoefficient*1000),"&plusmn;",((cRef*1000)-((tRef-273.15)*(HSA*(slopecoefficient*1000))))))
    else:
        if plotMolWeight: 
            hsaForm = ("<i>%.2f%s18.9 &middot; (273.15 - T) + %.0f%s1353</i>" % (HSA*(slopecoefficient*1000),"&plusmn;",(interceptcoefficient*(mw*1000)),"&plusmn;"))
            mwForm = ("<i>%.2f%s13 &middot; (273.15 - T) + %.0f%s540</i>" % (mw*(mwSlope*1000),"&plusmn;",(mwIntercept*(mw*1000)),"&plusmn;"))
            return hsaForm,mwForm
        else: return ("<i>%.2f%s18.9 &middot; (273.15 - T) + %.0f%s1353</i>" % (HSA*(slopecoefficient*1000),"&plusmn;",(interceptcoefficient*(mw*1000)),"&plusmn;"))
    
"""Function that plots the linear increase in heat capacity of a folded protein.
        Inputs:
            x       - list of x-values (floats) to plot that should have the same size as y and consistent indexes
            y       - list of y-values (floats) to plot that should have the same size as x and consistent indexes
            plotFile- optionally define an output file
            show    - boolean, show the plot in a graphical window
            startT  - start temperature in Celsius to plot
            stopT   - stop temperature in Celsius to plot
        Outputs:
            -
"""
def plotHeatCapacity(x,y,plotFile="out",show=False,celsius=False,x2=[],y2=[],plotTitle="",heatcapFile="",tRef=0.0,cRef=0.0):
    #Set the font size
    pyplot.rcParams.update({"font.size":12})

    #Is the start temperature higher than stop?
    if len(x) != len(y):
        sys.stderr.write("Length of x and y coordinates must be the same length if you want a plot.\n")
        exit(-1)
    if len(x2) != len(y2):
        sys.stderr.write("Length of x2 and y2 coordinates must be the same length if you want a plot.\n")
        exit(-1)
    
    #Figure size and make sure the legend is out of the box    
    fig = pyplot.figure(figsize=(12,7.5))
    ax = fig.add_subplot(111)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    if len(plotTitle) > 0: pyplot.title(plotTitle)

    #Plot the function
    ax.plot(x,y,label="Slope calculated\nwith HSA")
    
    #Proxy artist to add the standard deviation to the legend
    p = pyplot.Rectangle((0, 0), 0, 0,label="HSA, 1 s.d.",alpha=0.1)
    ax.add_patch(p)

    p = pyplot.Rectangle((0, 0), 0, 0,label="HSA, 2 s.d",alpha=0.05)
    ax.add_patch(p)
    
    #Calculate the upper and lower bounds of the error
    posYErr = []
    negYErr = []
    posYErrT2 = []
    negYErrT2 = []
    for index in range(0,len(x)):
        if tRef != 0.0:
            posYErr.append((y[index]*1000+(18.91575*fabs(tRef-x[index])))/1000)
            negYErr.append((y[index]*1000-(18.91575*fabs(tRef-x[index])))/1000)
            posYErrT2.append((y[index]*1000+((18.91575*2)*fabs(tRef-x[index])))/1000)
            negYErrT2.append((y[index]*1000-((18.91575*2)*fabs(tRef-x[index])))/1000)
        else:
            posYErr.append((y[index]*1000+(18.91575*fabs(273.15-x[index])+1353.032))/1000)
            negYErr.append((y[index]*1000-(18.91575*fabs(273.15-x[index])+1353.032))/1000)
            posYErrT2.append((y[index]*1000+((18.91575*2)*fabs(273.15-x[index])+(1353.032*2)))/1000)
            negYErrT2.append((y[index]*1000-((18.91575*2)*fabs(273.15-x[index])+(1353.032*2)))/1000)

    #Add shaded area (=error)
    ax.fill_between(x,posYErr,negYErr,color="blue",alpha=0.1,linewidth=0,label="HSA 1 standarddeviation")
    ax.fill_between(x,posYErrT2,negYErrT2,color="blue",alpha=0.05,linewidth=0,label="HSA 2 standarddeviations") 
    
    #Should we plot the freire baseline?
    if len(x2) > 0:
        #Same procedure as for the HSA baseline, see above
        ax.plot(x2,y2,color="red",alpha=0.5,linestyle="dashed",label="Freire's\nbaseline")
        posYErr2 = []
        negYErr2 = []
        posYErr2T2 = []
        negYErr2T2 = []
        for index in range(0,len(x2)):
            if tRef != 0.0:
                posYErr2.append((y2[index]*1000+(13*fabs(tRef-x2[index])))/1000)
                negYErr2.append((y2[index]*1000-(13*fabs(tRef-x2[index])))/1000)
                posYErr2T2.append((y2[index]*1000+((13*2)*fabs(tRef-x2[index])))/1000)
                negYErr2T2.append((y2[index]*1000-((13*2)*fabs(tRef-x2[index])))/1000)
            else:
                posYErr2.append((y2[index]*1000+(13*fabs(273.15-x2[index])+540))/1000)
                negYErr2.append((y2[index]*1000-(13*fabs(273.15-x2[index])+540))/1000)
                posYErr2T2.append((y2[index]*1000+((13*2)*fabs(273.15-x2[index])+(540*2)))/1000)
                negYErr2T2.append((y2[index]*1000-((13*2)*fabs(273.15-x2[index])+(540*2)))/1000)

        p = pyplot.Rectangle((0, 0), 0, 0,label="Freire's\nbaseline, 1 s.d.",color="red",alpha=0.1)
        ax.add_patch(p)
        p = pyplot.Rectangle((0, 0), 0, 0,label="Freire's\nbaseline, 2 s.d.",color="red",alpha=0.05)
        ax.add_patch(p)
 
        ax.fill_between(x2,posYErr2,negYErr2,color="red",alpha=0.1,linewidth=0) 
        ax.fill_between(x2,posYErr2T2,negYErr2T2,color="red",alpha=0.05,linewidth=0) 

    #Is there a file specified with experimental data?
    if len(heatcapFile) > 0:
        #Try to open the experimental data file
        try:
            heatcap = open(heatcapFile)
        except IOError:
            sys.stderr.write("Could not open the experimental heat capacity data.\n")
            exit(-1)
        
        #Init vars that will contain the coordinates
        heatcapT = []
        heatcapC = []
        
        #Iterate over lines and extract information
        for line in heatcap:
            if line.startswith("#"): continue
            try:
                    heatcapT.append(float(line.split("\t")[0].strip()))
                    heatcapC.append(float(line.split("\t")[1].strip()))
            except ValueError:
                    sys.stderr.write("Could not extract the experimental heat capacity data measurements.\n")
                    sys.stderr.write("The experimental data contains a value that could not be interpreted as a number.\n")
                    sys.stderr.write("Could not interpret the following line to contain numbers: %s\n" % (line.rstrip()))
                    if "," in line: sys.stderr.write("Please use decimal dot (\".\") for numbers and \"\\t\" for seperating the values.\n")
                    exit(-1)
            except IndexError:
                    sys.stderr.write("Please make sure the data is tab-seperated and contains at least 2 columns.\n")
                    sys.stderr.write("The first column should contain the temperature in K and the second the heat capacity in kJ mol^-1 K^-1.\n")
                    sys.stderr.write("Could not parse the following line: %s\n" % (line.strip()))
                    exit(-1)
        if len(heatcapC) < 1:
            sys.stderr.write("The experimental data that was supplied does not contain any datapoints to plot.\n")
            sys.exit(-1)

        #Make sure the lengths are the same, they are coordinates so they should be of equal length
        if len(heatcapC) != len(heatcapT):
            sys.stderr.write("Length of heat capacity and heat capacity temperature measurements must be the same length if you want to plot.\n")
            exit(-1)
        
        #Sort the points on temperature
        heatcapC = [tempx for tempx in heatcapC]
        heatcapC = [tempHeatcapC for (tempHeatcapT,tempHeatcapC) in sorted(zip(heatcapT,heatcapC))]
        heatcapT.sort()

        #Plot the experimental data
        ax.plot(heatcapT,heatcapC,label="Heat capacity",marker="o",color="magenta")

    #Define axis labels
    pyplot.ylabel("Heat capacity (k$J$ $K^{-1}$ mol$^{-1}$)")
    
    #Set the labels it should use
    if celsius: pyplot.xlabel("Temperature ($$^\circ$C$)")
    else:       pyplot.xlabel("Temperature ($K$)")

    ax.legend(loc="center left",bbox_to_anchor=(1.0,0.5),prop={"size":12})
    
    #Save the figure in pdf and png format
    try:
        fig.savefig(plotFile+".pdf", format="pdf",transparent=True)
    except Exception,e:
        sys.stderr.write("Could not write the plot to a file\n")
        sys.stderr.write("Following error message was generated:\n")
        sys.stderr.write(str(e)+"\n")
        exit(-1)

    try:
        fig.savefig(plotFile+".png")
    except Exception,e:
        sys.stderr.write("Could not write the plot to a file\n")
        sys.stderr.write("Following error message was generated:\n")
        sys.stderr.write(str(e)+"\n")
        exit(-1)
    pyplot.close()
