#Author:            Robbin Bouwmeester (supervisors: Erik van Dijk & Sanne Abeln, feedback and server implementation: Bernd Brandt)
#Purpose:           Visualizing the Hydrophobic Surface Area (HSA) and writing 3 figures to a file.
#Date:              19-06-2014
#Python version:    2.7.5
#Packages:          Matplotlib V1.3.1
#Bugs:              No bugs found...

#Imports
import pymol
import sys
from cStringIO import StringIO
import os

"""Function that runs the program DSSP on a pdb file.

    Inputs:
        pdbFile       - define the location of the pdb input file
        removeChains  - list that contains chains that should be removed for visualization
        figPrefix     - prefix for figure names
    
    outputs:
        three png images
"""
def saveFig(pdbFile,removeChains=[],figPrefix="structure",pathF=""):
    #Start pymol in quiet mode
    pymol.pymol_argv = ['pymol','-qc']
    pymol.finish_launching()
    cmd = pymol.cmd
    
    #Load PDB file
    cmd.load(pdbFile)

    #Set background colour
    cmd.bg_color("white")
    
    #Hide all AA
    cmd.hide("all")
    
    #Colour the surface blue
    cmd.color("blue")
    
    #Remove chains
    for val in removeChains:
         cmd.remove("chain %s" % (val))
    
    #Show the surface
    cmd.show("surface")

    #Colour hydrophobic AA in red
    cmd.color("red","resn ala+phe+cys+leu+ile+trp+val+met+tyr")
    
    #Try to fit the figure to canvas size    
    cmd.zoom(complete=1,buffer=0.0)   

    #Save image and turn around the protein
    cmd.ray()
    cmd.png(os.path.join(pathF,figPrefix+".png"),width=0,height=0,ray=0)
    cmd.turn("y",-120)
    cmd.ray()
    cmd.png(os.path.join(pathF,figPrefix+"120.png"),width=0,height=0,ray=0)
    cmd.turn("y",240)
    cmd.ray()
    cmd.png(os.path.join(pathF,figPrefix+"240.png"),width=0,height=0,ray=0)
    
    #Reset PyMol
    cmd.reinitialize()
