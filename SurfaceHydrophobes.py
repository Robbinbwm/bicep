#!/usr/bin/python

import string
import math
import re
import sys
import operator

from PDBStruc import *

############################


def countSurfaceResidues(pdbStructure):
    # read maximum surface area
    a = AccUnfold()
    a.readAccUnfold()

    
    statChain = {}
    for chain_num in  pdbStructure.chains.keys():
        #  initialise counting variables
        totcountAll =0
        totaccAll =0
        totcountSurface=0
        totaccSurface=0
        totcountSurfaceHydrophobes=0
        totaccSurfaceHydrophobes=0
        #Iterate over the residues
        for res_num in pdbStructure.chains[chain_num].residues.keys():
            residue = pdbStructure.chains[chain_num].residues[res_num]
            #Available DSSP information?
            if residue.resDSSP:
                AA = residue.resDSSP.aa_type
                acc = residue.resDSSP.acc
                buried = a.isBuried(AA,acc)
                hydrophobic = isHydrophobic(AA)

                totcountAll +=1
                totaccAll += acc
                #Only count non-buried residues
                if not buried:
                    totcountSurface +=1
                    totaccSurface += acc
                    if hydrophobic:
                        totcountSurfaceHydrophobes +=1
                        totaccSurfaceHydrophobes += acc
        statChain[chain_num] = [totcountAll,totaccAll,totcountSurface,totaccSurface,totcountSurfaceHydrophobes,totaccSurfaceHydrophobes]
    return statChain
    

################################3

def isHydrophobic(AA):
    HydrophobeList= ['A'  ,'C'  ,'F'  ,'L'  ,'I'  ,'W'  ,'V'  ,'M'  ,'Y']
    ans= False
    if AA in HydrophobeList:
        ans = True
    return ans


##############################


def getHSA(PDB_ID,hdir = "./",PDB_file = "",DSSP_file = ""):
    p = PDBStructure(PDB_ID)
    if (len(PDB_file) > 0 and len(DSSP_file) > 0): 
        p.readPDBfile(PDB_file)
        p.readDSSPfile(DSSP_file)
    else:
        p.readPDBfile(hdir+PDB_ID+".pdb")
        p.readDSSPfile(hdir+PDB_ID+".dssp")
    returnTuple = countSurfaceResidues(p)
    return returnTuple


###########################

def main():
    getHSA(sys.argv[1])

if __name__ == "__main__":
  sys.exit(main())
