# README #

### What is this repository for? ###

This repository contains the code that is also available at the webserver: www.ibivu.nl/programs/bicepwww

### How do I get set up? ###

* The python code should be copied in any directory
* Install python Matplotlib (preferably version 1.3.1 or later)
* Install python PyMOL (preferably version 1.1 or later)

### How do I run the code? ###

* The code can be run by calling: "HydrophobicEffectWrapper.py"
* All the command line option can be made visible by running:
       
```
#!terminal

./HydrophobicEffectWrapper.py --help
```

* An example of a run is:

```
#!terminal

./HydrophobicEffectWrapper.py -s 2PDD.pdb -c A -f
```

* All the output files will be written to the same directory as the PDB file, **files with the same name as output files will be overwritten.**
* Please see the usecases in the manual for information about the interpretation: www.ibivu.nl/programs/bicepwww

### Who do I talk to? ###

* Please contact: s.abeln@vu.nl