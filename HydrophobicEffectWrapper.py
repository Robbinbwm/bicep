#!/usr/bin/python

#Author:            Robbin Bouwmeester (supervisors: Erik van Dijk & Sanne Abeln, feedback and server implementation: Bernd Brandt)
#Purpose:           Wrapper for calculating the linear increase in heat capicity of folded proteins
#                   also plotting data and getting some statistics
#Date:              13-11-2014
#Python version:    2.7.5
#Packages:          Matplotlib V1.3.1, PyMol 1.1 or newer
#Bugs:              No bugs found...

#Imports
import sys
import SurfaceHydrophobes
from GetMolWeight import getMolecularWeight
from runDSSP import runDSSP
from calculateHC import calculateFormulaHC
from os.path import isfile
from pymolHSA import saveFig
from extractChainFromPDB import extractChain, checkConsistsAA
import copy
import itertools

from optparse import OptionParser

import os


##################################################################################################
#Make sure we log all the stderr, use standard logging library with some extension               #
#The code below (till "END COPY CODE") was taken from a blog by Ferry Boender with a GPL license:#
# http://www.electricmonk.nl/log/2011/08/14/redirect-stdout-and-stderr-to-a-logger-in-python/    #
##################################################################################################
import logging

class StreamToLogger(object):
   """
   Fake file-like stream object that redirects writes to a logger instance.
   """
   def __init__(self, logger, log_level=logging.INFO):
      self.logger = logger
      self.log_level = log_level
      self.linebuf = ''
 
   def write(self, buf):
      for line in buf.rstrip().splitlines():
         self.logger.log(self.log_level, line.rstrip())

def setLoggerToFile(outfile="dssp.err",append=True):
    if append == True: logmode = "a"
    else: logmode = "w"
    #Set some basic params for logging stderr
    logging.basicConfig(
       level=logging.DEBUG,
       format="%(asctime)s:%(levelname)s:%(name)s:%(message)s",
       filename=outfile,
       filemode=logmode
    )

    #Make a logger object for the stderr
    stderr_logger = logging.getLogger('STDERR')

    #Define an (write-) object where we write the log to
    sl = StreamToLogger(stderr_logger, logging.ERROR)

    #Set the stderr to the defined object
    return sl

#################
# END COPY CODE #
#################

"""Function that creates an option parser
        Inputs:
            -
        Outputs:
            options - dictionary that contains the options parsed that were supplied by the user
"""
def parse_commandline():
    usage = "%prog <PDB> [options]"
    version = "1.0a"
    description = \
        "Wrapper for calculating the baseline heat capacity of native proteins using the Hydrophobic Surface Area"
    epilog = \
        "Option Parser Copyright (c) 2014 Robbin Bouwmeester"
    parser = OptionParser(usage=usage, description=description,
                          version="%prog "+version, epilog=epilog)

    #Parse the commandline options
    parser.add_option("-s", "--structure",  dest="pdbFile", metavar="<file>",
                     help="input protein structure (PDB/ENT)")
    parser.set_defaults(pdbFile="")
    
    parser.add_option("-c", "--chains",  dest="chains", 
                     help="Select chains that are present in the PDB/ENT file, without defining chains it will analyze all chains present in the file")
    parser.set_defaults(chains="")
    
    parser.add_option("-e", "--experimental",  dest="heatcapF",
                     help="Define a tab seperated file with the first column specifying the temperature (in Kelvin) and the second column the heat capacity (in kJ mol^-1 K^-1)")
    parser.set_defaults(heatcapF="")

    parser.add_option("-x", "--referenceT",  dest="tRef",
                     help="Optionally define a reference temperature in Kelvin for a known point of interception with the corresponding heat capacity")
    parser.set_defaults(tRef=0.0)
    
    parser.add_option("-y", "--referenceC",  dest="cRef",
                     help="Optionally define a reference heat capacity (in kJ mol^-1 K^-1) for a known point of interception with the corresponding temperature")
    parser.set_defaults(cRef=0.0)    

    parser.add_option("-f", "",  dest="plotMolWeight",  action="store_true",
                     help="Should the plot contain a baseline estimation based on Freire's molecular weight based model. Include this flag if it should")
    parser.set_defaults(plotMolWeight=False)
    
    
    
    #Get the options:
    (options, args) = parser.parse_args()
    
    if len(options.pdbFile) == 0:
        sys.stderr.write("PDB or ENT file not specified, will crash. Use the -s parameter to specify a file.\n")
        sys.exit(-1)
    try:
        float(options.tRef)
        float(options.cRef)
    except ValueError:
        sys.stderr.write("Could not fix the baseline through a point in the graph.\n")
        sys.stderr.write("The reference temperature or heat capacity is not a number.\n")
        sys.exit(-1)
    
    #Make sure the reference points are properly formatted
    if options.tRef != 0.0 or options.cRef != 0.0:
        if options.cRef == 0.0:
            sys.stderr.write("Could not fix the baseline through a point in the graph.\n")
            sys.stderr.write("The reference temperature was defined but no heat capacity was defined, please define both.\n")
            sys.exit(-1)
        if options.tRef == 0.0:
            sys.stderr.write("Could not fix the baseline through a point in the graph.\n")
            sys.stderr.write("The reference heat capacity was defined but no temperature was defined, please define both.\n")
            sys.exit(-1)
        if options.tRef < 0.0:
            sys.stderr.write("Could not fix the baseline through a point in the graph.\n")
            sys.stderr.write("The reference temperature cannot be below 0 kelvin.\n")
            sys.exit(-1)
        if options.cRef < 0.0:
            sys.stderr.write("Could not fix the baseline through a point in the graph.\n")
            sys.stderr.write("The reference heat capacity cannot be below 0.\n")
            sys.exit(-1)

    #Check for any leftover command line arguments:
    if len(args):
        sys.stderr.write("Ignoring additional arguments %s.\n" % (str(args)))
    
    #Clean up
    del(parser)
    return options

def main():
    #The first argument should be the location of a PDB file
    options = parse_commandline()
    pdbFile = options.pdbFile
    chainsToAnalyze = options.chains
    heatcapF = options.heatcapF
    plotMolWeight = options.plotMolWeight

    tRef = float(options.tRef)
    cRef = float(options.cRef)

    #Does the PDB file exist?    
    if not isfile(pdbFile):
        sys.stderr.write("Could not find the input PDB file, will exit.\n")
        exit(-1)
    
    tailDir,fileN = os.path.split(os.path.abspath(pdbFile))

    #Define valid chain names
    validChainCharacters = set("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890")
    
    #Chains specified for analysis?
    if len(chainsToAnalyze) > 0:        
        #Test if characters for chains are valid
        if not(set(chainsToAnalyze).issubset(validChainCharacters)): 
            sys.stderr.write("The chains specified contains illegal characters.\n")
            exit(-1)
        if len(chainsToAnalyze) < 1:
            sys.stderr.write("No chains were specified to analyze.\n")
            exit(-1)
        
        #Write chains to temporary file
        try:
            extractChain(pdbFile,chainsToAnalyze,outfileN=os.path.join(tailDir,"temp.pdb"))
            pdbFile = os.path.join(tailDir,"temp.pdb")
        except IOError:
            sys.stderr.write("Could not extract chains from the given PDB file.\n")
            sys.exit(-1)
    else:
        nonAAChains, nonAA = checkConsistsAA(pdbFile)
        if len(nonAAChains) > 0:
            sys.stderr.write("PDB structure contains non-AA residues.\n")
            sys.stderr.write("The following chain(s) contain non-AA residues: %s\n" % (" ".join(nonAAChains)))
            sys.stderr.write("Chains contain the following non-AA residues: %s\n" % (" ".join(nonAA)))
            sys.exit(-1)

    dsspLogger = setLoggerToFile()
    try:
        #Run DSSP
        nonFatErr = runDSSP(pdbFile,os.path.join(tailDir,"dssp.out"),"%s/dsspcmbi" % os.path.dirname(os.path.realpath(__file__)))
        dsspLogger.write(nonFatErr)
        dsspFile = os.path.join(tailDir,"dssp.out")
    except Exception,e:
        sys.stderr.write("Unable to run DSSP\n")
        sys.stderr.write("Following error message was generated:\n")
        sys.stderr.write(str(e)+"\n")
        exit(-1)
        
    try:
        #Get the hydrophobic surface area
        surfaceStats = SurfaceHydrophobes.getHSA(pdbFile,PDB_file=pdbFile,DSSP_file=dsspFile)
    except Exception,e:
        sys.stderr.write("Not able to get the hydrophobic surface area.\n")
        sys.stderr.write("Following error message was generated: \n")
        sys.stderr.write(str(e)+"\n")
        exit(-1)
    
    try:
        #Get the molecular weight
        mwChains = getMolecularWeight(pdbFile,PDB_file=pdbFile)
    except Exception,e:
        sys.stderr.write("Not able to get the molecular weight.\n")
        sys.stderr.write("Following error message was generated: \n")
        sys.stderr.write(str(e)+"\n")
        exit(-1)
    
    #Write a linear increase in heat capacity of a folded protein to a file
    try:
        
        #Need at least one chain
        if len(chainsToAnalyze) > 0:
            if not(set(chainsToAnalyze).issubset(surfaceStats.keys())):
                sys.stderr.write("Supplied chain(s) could not be found in the structure.\n")
                sys.stderr.write("Used specified chains: %s\n" % (" ".join(chainsToAnalyze)))
                sys.stderr.write("Available chains in structure: %s\n" % (" ".join(surfaceStats.keys())))
                sys.exit(-1)
            chains = chainsToAnalyze
        else:
            chains = surfaceStats.keys()
        
        #Sum the HSA and mw for the specified chains
        HSA = 0.0
        mw = 0.0
        for key in chains:
             HSA += surfaceStats[key][5]
             mw += mwChains[key]
             #If there are no amino acids... Skip!
             if mw == 0:
                  chains.remove(key) 
                  continue

        #Get the formula for the specified chains
        plotTitle = "Heat capacity baseline estimation for %s with chain(s) %s" % (fileN.replace(".pdb","").replace(".ent","").upper()," ".join(chains))

        if plotMolWeight:
            hsaForm, mwForm = calculateFormulaHC(HSA,mw,plotFile=os.path.join(tailDir,"out"),plotTitle=plotTitle,heatcapFile=heatcapF,plotMolWeight=plotMolWeight,tRef=tRef,cRef=cRef)
            print "Equation obtained for %s with chain(s) %s.\n" % (fileN.replace(".pdb","").replace(".ent","")," ".join(chains))
            print "Hydrophobic Surface Area based method: <i>C</i> = %s\n" % (hsaForm)
            print "Freire baseline equation: <i>C</i> = %s\n" % (mwForm)
        else:
            hsaForm = calculateFormulaHC(HSA,mw,plotFile=os.path.join(tailDir,"out"),plotTitle=plotTitle,heatcapFile=heatcapF,plotMolWeight=plotMolWeight,tRef=tRef,cRef=cRef)
            print "Equation obtained for %s with chain(s) %s: <i>C</i> = %s\n" % (fileN.replace(".pdb","").replace(".ent","")," ".join(chains),hsaForm)
                       
        print "Calculated Hydrophobic Surface Area: %.2f &#8491;&sup2;\n" % (HSA)
        print "Calculated molecular weight: %.2f u\n" % (mw)
        
        #Save a image where the HSA is visualized
        try:
            saveFig(pdbFile,figPrefix="structure_%s_%s" % (fileN.replace(".pdb","").replace(".ent",""),"".join(chains)),removeChains=set(surfaceStats.keys()).difference(set(chains)),pathF=tailDir)
        except Exception,e:
            sys.stderr.write("Not able to create the protein visulization.\n")
            sys.stderr.write("Following error message was generated: \n")
            sys.stderr.write(str(e)+"\n")
    except Exception,e:
        sys.stderr.write("Not able to calculate the linear increase or generate the plot.\n")
        sys.stderr.write("Following error message was generated: \n")
        sys.stderr.write(str(e)+"\n")
        exit(-1)
    exit(0)
    
if __name__ == "__main__":
    main()
