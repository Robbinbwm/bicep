#Author:            Robbin Bouwmeester (supervisors: Erik van Dijk & Sanne Abeln, feedback and server implementation: Bernd Brandt)
#Purpose:           Extracting a specific chain from a PDB file. Only extract the lines with the header ATOMS. Other headers are
#                   ignored. Also a function that checks if the PDB file contains non-AA residues.
#Date:              19-06-2014
#Python version:    2.7.5
#Packages:          Matplotlib V1.3.1
#Bugs:              No bugs found...

#Imports
import sys

"""Function that extracts chains from a PDB file and checks if it contains only AA
        Inputs:
            infileN  - location of PDB file
            chains   - list with chains it should extract
            outfileN - name for PDB file where the extracted chains should be written to
        Outputs:
            -
"""
def extractChain(infileN,chains,outfileN="temp.pdb"):
    #Check if chains only contains AA
    nonAAChains, nonAA = checkConsistsAA(infileN)
    if len(set(nonAAChains).intersection(set(chains))) > 0:
        sys.stderr.write("The specified chains contains a chain that is non-AA, specify other chains or remove the non-AA coordinates and the SEQRES header from the PDB file.\n")
        sys.stderr.write("Chains containing non-AA: %s\n" % " ".join(list(set(nonAAChains).intersection(set(chains)))))
        sys.stderr.write("Chains contain the following non-AA residues: %s\n" % (" ".join(nonAA)))
        sys.exit(-1)
    
    #Open PDB file
    try:
        infile = open(infileN)
    except IOError:
        sys.stderr.write("Could not open the initial PDB file, chain extraction failed.\n")
        sys.exit(-1)
    
    #Open outfile
    try:
        outfile = open(outfileN,"w")
    except IOError:
        sys.stderr.write("Could not write to a temporary PDB file, chain extraction failed.\n")
        sys.exit(-1)
    
    #Make sure we have written at least one line
    writtenChainInfo = False
    
    presentChains = []    

    #Iterate over infile
    for line in infile:
        #Extract the chain data
        if line.startswith("ATOM") or line.startswith("TER"):
            chain = line[21]
            presentChains.append(chain)
            if chain in chains:
                writtenChainInfo = True
                outfile.write(line)
        else:
            outfile.write(line)

    #Did we write some chain information? If not -> crash
    if not(writtenChainInfo):
        sys.stderr.write("Supplied chain(s) could not be found in the structure.\n")
        sys.stderr.write("Used specified chains: %s\n" % (" ".join(chains)))
        sys.stderr.write("Available chains in structure: %s\n" % (" ".join(set(presentChains).difference(set(nonAAChains)))))
        sys.exit(-1)

"""Function that check if the SEQRES header contains non-AA residues 
        Inputs:
            infileN       - location of PDB file
        Outputs:
            invalidChains - list with chains that contain non-AA residues
"""
def checkConsistsAA(infileN):
    #Open PDB file
    try:
        infile = open(infileN)
    except IOError:
        sys.stderr.write("Could not open the initial PDB file, chain extraction failed.\n")
        sys.exit(-1)
    
    #Known 3-letter amino acids
    existAA = ["ALA","ARG","ASN","ASP","ASX","CYS","GLU","GLN","GLX","GLY","HIS","ILE","LEU","LYS","MET","PHE","PRO","SER","THR","TRP","TRY","TYR","VAL","SYC",""]
    nonAA = ["DC","DT","DG","DA","DI","DU","U","A","C","T","G","I"]
    #Vars that will contain all chains that contain non-AA residues and the invalid residue
    invalidChains = []
    invalidRes = []

    #Iterate over PDB file and check if the sequence contains non-AA residues
    for line in infile:
        if line.startswith("SEQRES"):
             chain = line[11]
             seq = line[18:].strip().upper()
             #if set(seq.split(" ")).issubset(existAA):
             if len(set(seq.split(" ")).intersection(nonAA)) > 1:
                 invalidChains.append(chain)
                 invalidRes.extend(set(seq.split(" ")).difference(existAA))
    
    #Return list with chains that contain non-AA residues
    return list(set(invalidChains)), list(set(invalidRes))
