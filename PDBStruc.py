#execfile("PDBStruc.py")

import string
import math
import re
import sys

class PDBStructure:
    """
    PDBStructure class that holds structures from PDB files
    
    instance attributes:
    file_name
    resList
    DSSPInfo
    """
    # class attributes
    boolDSSP = True
    boolHBplus = True
    PDBdir = "."
    strictPDB = False



    def __init__(self, pdbID, filename= None, DSSP = None, HBplus = None, chains = None):

        self.filename = filename        
        self.pdbID = pdbID
        if chains is None:
            self.chains = {}
        else:
            self.chains = chains
        self.DSSP = DSSP
        self.HBplus = HBplus


    def readPDBfile(self, filename=None):
        if(filename is not None):
            self.__readPDBfile__(filename)
        elif(self.filename is not None):
            self.__readPDBfile__(self.filename)
        else:
            self.__readPDBfile__(PDBdir + pdbID)
 

    def __readPDBfile__(self,filename):
        try:
                infile = open(filename, 'r') 
        except IOError: # In case of IOError return empty collection
                sys.stderr.write("Error: Cannot open 2 PDB file %s.\n" % (filename))
                exit(-1)
        for line in infile.readlines():
            # not captured: alternative locations
            # rstrip remove the "\n" from the line
            line =  line.rstrip()
            # take the first 4 characters of the line
            first4 = line[0:4]
            #test if atom or hetatom
            if(first4 == "ATOM"):
                #get atom number
                atom_num = int(line[7:11])
                # get atom type
                atom_type = line[12:16].strip()
                # get amino acid type
                aa_type = line[17:20].strip()
                # get residue number
                res_num  = int(line[22:26])
                # chain in PDB file
                chain = line[21]
                # get the coordinates
                xcoord = float(line[30:38])
                ycoord = float(line[38:46])
                zcoord = float(line[46:54])


                atom = Atom(atom_type,atom_num,xcoord,ycoord,zcoord)
                self.addAtom(chain,res_num,aa_type,atom)
        infile.close()

    def readDSSPfile(self,filename):
        self.__readDSSPfile__(filename)


    def __readDSSPfile__(self,filename):
        try:
            infile = open(filename, 'r') 
        except IOError: # In case of IOError return empty collection
            sys.stderr.write("Error: Cannot open 3 PDB file %s.\n" % (filename))
            exit(-1)
        start_reading = False
        for line in infile.readlines():
            # rstrip remove the "\n" from the line
            line =  line.rstrip()
            
            first4 = line[0:4]
            if(first4 == "  # "):   
                start_reading = True
            elif(start_reading):	
                # get information from the DSSP file, see 
                # http://swift.cmbi.ru.nl/gv/dssp/
                
                # get amino acid type
                aa_type = line[13]

                # skip amino acids marked '!'
                if(aa_type == '!'):
                        continue


                # get residue number
                res_num  = int(line[5:10].strip())

                if(aa_type.islower()):
                    sys.stderr.write("cysteine bridge residue: (%s,%s)\n" %  (str(res_num), str(aa_type)))
                    aa_type = "C"

                # get chain 
                chain_num = line[11]
                
                # get surface accessible area
                try: acc = int(line[34:38].strip())
                except:
                    sys.stderr.write("Could not get the accessible surface area for position %s in chain %s, probably due to bad formatting of the DSSP file.\n" % (res_num,chain_num))
                
                # get secondary structure assignment
                sse = line[16]

                # store DSSP info
                
                dsspRes = ResDSSP(res_num, aa_type, sse,acc)
                try:
                    self.addResDSSP(dsspRes, chain_num)
                except PDBError:
                    sys.stderr.write("PDBError:\n")
                    sys.stderr.write(line+"\n")
                    raise
                
                # end if ATOM

        # end loop readlines()	
	# close the infile
	infile.close()

    def addResDSSP(self, dsspRes, chain_num):
        res_num = dsspRes.res_num
        if dsspRes in ["!","X","x","?"]: return      
        if(chain_num not in self.chains): return
        if(res_num not in self.chains[chain_num].residues): return
      
        self.chains[chain_num].residues[res_num].addResDSSP(dsspRes)



    def addAtom(self,chain_num,res_num,aa_type,atom):
        #check if chain exists
        if(chain_num not in self.chains):
            c=Chain(chain_num)
            self.addChain(c)

        # check if residue exists
        if(res_num not in self.chains[chain_num].residues):
            r = Residue(res_num, aa_type)
            self.chains[chain_num].addResidue(r)
            
        #add atom to structure
        self.chains[chain_num].residues[res_num].addAtom(atom)


    def addChain(self,chain):
        ''' expects object of type Chain'''
        self.chains[chain.chain_num]=chain
        
    def readHBplusFile(self,filename):
        self.__readHBplusFile__(filename)

    def __readHBplusFile__(self,filename):
        self.__init__hbplus__()

        try:
            infile = open(filename, 'r') 
        except IOError: # In case of IOError return empty collection
            sys.stderr.write("Error: Cannot open 4 PDB file %s.\n" % (filename))
            exit(-1)
        start_reading = False
        for line in infile.readlines():
            # rstrip remove the "\n" from the line
            line =  line.rstrip()
            
            first6 = line[0:6]
            if(first6 == "n    s"):   
                start_reading = True
            elif(start_reading):
                
                donorChain = line[0]
                donorResnum = int(line[1:5])
                donorAA  = line[6:9]
                donorAtom = line[10:13].strip()
                donorClass = line[33]
                
                acceptorChain =line[14]
                acceptorResnum = int(line[15:19])
                acceptorAA = line[20:23]
                acceptorAtom = line[24:27].strip()
                acceptorClass = line[34]
                
                # add hbonds 
                self.addHbond(acceptorChain,acceptorResnum,donorChain,donorResnum,acceptorAA, "acceptor",acceptorClass)
                self.addHbond(donorChain,donorResnum,acceptorChain,acceptorResnum, donorAA, "donor",donorClass)

    def addHbond(self,chain_num,res_num,partner_chain, partner_res_num,aa_type,type_da,type_MS):
        if(chain_num == '-' or partner_chain== '-'):
            # ignore water h-bonds
            return
        #check if chain exists
        if(chain_num not in self.chains):
            raise PDBError("warning: chain does not exist " + chain_num)
        # check if residue exists
        if(res_num not in self.chains[chain_num].residues):
            raise PDBError("warning: residue does not exist " + res_num) 
        #check if aa types match
        aa = self.chains[chain_num].residues[res_num].aa
        if(aa_type  !=  aa):
            raise PDBError("warning: aa type does not match " + aa_type + " " + aa)
        #insert hbond
        self.chains[chain_num].residues[res_num].resHBplus.addHbond(partner_chain+str(partner_res_num),aa_type,type_da,type_MS)
            

    def __init__hbplus__(self):
        for chain_num in  self.chains.keys():
            for res_num in self.chains[chain_num].residues.keys():
                self.chains[chain_num].residues[res_num].initResHBplus()
                

# end class PDBStructure


class Chain():
    '''Chain keeps residues
    ''' 
    def __init__(self,chain_num,residues=None):
        if(residues is None):
            self.residues={}
        else:
            self.resiodues=residues
        self.chain_num=chain_num


    def addResidue(self,residue):
        self.residues[residue.res_num]=residue

    def __str__(self):
        out = ""
        out = out + "chain num "+self.chain_num+"\n"
        return out + string.join( map(str,self.residues.values()),"\n")


    def calculateContacts(self,cutoff=7.0,sequenceDist=2,atomType='CA',contactMatrix = None):
        print "Calculating contacts:"
        print "distance cutoff", cutoff, "sequence dist",sequenceDist, \
            "atomType", atomType
        sqCutoff = cutoff**2.0
        defaultAtomType= 'CA'
        resnums = self.residues.keys()
        for i in resnums:
            if( atomType in self.residues[i].atoms):
                at = atomType
            else:
                at = defaultAtomType
            xi =  self.residues[i].atoms[at].x
            yi =  self.residues[i].atoms[at].y
            zi =  self.residues[i].atoms[at].z
            for j in resnums:
                if(j<(i-sequenceDist)): 
                    if( atomType in self.residues[j].atoms):
                        at = atomType
                    else:
                        at = defaultAtomType
                    xj =  self.residues[j].atoms[at].x
                    yj =  self.residues[j].atoms[at].y
                    zj =  self.residues[j].atoms[at].z
                    sqDist = (xi-xj)**2 + (yi-yj)**2 + (zi-zj)**2
                    # check if residues make contact
                    if(sqDist < sqCutoff):
                        weight = 0.0
                        if(contactMatrix == None):
                            weight = 1.0
                        else:
                            aa_i=self.residues[i].aa
                            aa_j=self.residues[j].aa
                            weight = contactMatrix.getWeight(aa_i,aa_j)

                        self.residues[i].contacts.append((j,weight))
                        self.residues[j].contacts.append((i,weight))
               




class Residue:
    '''Residue keeps atoms in dict
    ''' 
    def __init__(self, res_num, aa, atoms = None,resDSSP = None,resHBplus=None):
        self.res_num = res_num
        self.aa = aa
        self.resDSSP = resDSSP
        self.resHBplus = resHBplus
        self.contacts = []
        if(atoms is None):
            self.atoms={}
        else:
            self.atoms=atoms

    def addAtom(self,a):
        self.atoms[a.atom_type]=a

    def addResDSSP(self,resDSSP):
        self.resDSSP = resDSSP

    def initResHBplus(self):
        self.resHBplus = ResHBplus()

    def __str__(self):
        str_res = str(self.res_num)+' '+str(self.aa)
        str_hbplus = ""
        str_dssp = ""
        if(self.resHBplus is not None):
            str_hbplus = "   hbplus: " + str(self.resHBplus)
        if(self.resDSSP is not None):
            str_dssp = "   dssp: " + str(self.resDSSP)
            
       
        str_atoms = "   atoms:" + str(self.atoms.keys())
        return string.join([str_res,str_atoms,str_hbplus,str_dssp], "\n")


class Atom:
    def __init__(self, atom_type,atom_num,x,y,z):
        self.atom_type = atom_type
        self.atom_num = atom_num
        self.x = x
        self.y = y
        self.z = z
        
    def __str__(self):
        return str(self.atom_type)+'('+str(self.x)+','+str(self.y)+','+str(self.z)+')'

#############################3

class ResDSSP:
    def __init__(self,res_num, aa_type, sse,acc):
        self.res_num = res_num
        self.aa_type = aa_type
        self.sse = sse
        self.acc = acc

    def __str__(self):
        return string.join([str(self.res_num),self.aa_type, self.sse,str(self.acc)]," ")


#######################


class ResHBplus:
    def __init__(self):
        self.numDonorsM=0
        self.numAcceptorsM=0
        self.donors=[]
        self.acceptors=[]

    def addHbond(self,partner,aa,type_da,type_MS):
        if(type_MS=="M" ):
            if(type_da=="donor"):
                self.numDonorsM = self.numDonorsM +1
                self.donors.append(partner)
            elif(type_da=="acceptor"):
                self.numAcceptorsM = self.numAcceptorsM +1
                self.acceptors.append(partner)

    def __str__(self):
        return string.join( map(str,[self.numDonorsM,self.numAcceptorsM,"d",self.donors,"a",self.acceptors])," ")


#########################################################

class AccUnfold:
    # class attributes
    default_fn_unfolded = "/opt/apps/heatcap/robbin/AccUnfold.data" 
    default_cutoff = 0.07

    def __init__(self,fn_unfolded=None,unfolded_acc=None,read_hard_coded=True):
        
        if read_hard_coded is not None:
            self.read_hard_coded = read_hard_coded
        
        if fn_unfolded is not None:
            self.fn_unfolded = fn_unfolded
        else:
            self.fn_unfolded = self.default_fn_unfolded 

        if unfolded_acc is None:
            self.unfolded_acc =dict()
        else:
            self.unfolded_acc=unfolded_acc


    
    def readAccUnfold(self, filename=None):
        if self.read_hard_coded:
            self.unfolded_acc = {"R":241,
                                "W":259,
                                "Y":229,
                                "K":211,
                                "F":218,
                                "M":204,
                                "Q":189,
                                "H":194,
                                "E":183,
                                "L":180,
                                "I":182,
                                "N":158,
                                "D":151,
                                "C":140,
                                "V":160,
                                "T":146,
                                "P":143,
                                "S":122,
                                "A":113,
                                "G":85,
                                "X":175,
                                "Z":175}
        else:
            if(filename is None):
                filename = self.fn_unfolded
            try:
                    infile = open(filename, 'r') 
            except IOError: # In case of IOError return empty collection
                    sys.stderr.write("Error: Cannot open 5 PDB file %s.\n" % (filename))
                    exit(-1)
            for line in infile.readlines():
                line.rstrip()
                # split into columns
                fields = line.split()
                aa = fields[0]
                acc = float(fields[2])
                # store unfolded accessibility value
                self.unfolded_acc[aa]=acc
            # close file
            infile.close()
	# end for line
	
	


    def isBuried(self,aa,acc):
        ans = False
        if acc <  self.default_cutoff * self.unfolded_acc[aa]:
            ans= True
        return ans
        
######################################################



##########################################################

class PDBError(Exception):
    """Base class for exceptions in this module."""
    pass

#######################################################
# START PROGRAMS
#

########## MAIN ##############

def main():
    testPDB()


if __name__ == "__main__":
  sys.exit(main())

